"use client"
import React, { useState, useEffect } from 'react'
import Link from 'next/link';
const Employee = () => {
    const [data, setData] = useState([])
    useEffect(() => {
        setTimeout(() => {
        (async () => {
            let response = await fetch('https://6457c4de1a4c152cf98a03a8.mockapi.io/api/crud/users')
            response = await response.json()
            setData(response);
        })();
    }, 1000);
    }, []);
  return (
    <>
    <div className="section-full p-t80 p-b50">
    <div className="container">
      <div className="section-head text-center">
        <h2 className="text-uppercase">Employee details </h2>
      </div>
      <div className="section-content m-b30">
        <div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                <th scope="col">Code</th>
                <th scope="col">Name</th>
                <th scope="col">Designation</th>
                <th scope="col">Contact Details</th>
                <th scope="col">Work Profile</th>
              </tr>
            </thead>
            <tbody className="user_list">
            {data.map((item,i) => (
                <tr key={i}>
                    <th scope="row">{item.code}</th>
                    <td>{item.name}</td>
                    <td>{item.designation}</td>
                    <td>{item.mobile}</td>
                    <td>{item.profile}</td>
                </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
    </>
  )
}

export default Employee