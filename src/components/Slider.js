"use client"
import React from 'react'
import Image from 'next/image'
import Carousel from 'react-bootstrap/Carousel';

const Slider = () => {
  return (
    <>
      <div className="page-content">
    {/* SLIDER START */}
    <Carousel>
      {/* Wrapper for slides */}
      <Carousel.Item>
          <div className="slide-1">
            <div className="overlay" />
          </div>
          <div className="hero d-none d-md-block">
            <div className="text-left">
              <div className="text-left">
                <h1 className="text-white">
                  We Do Big Things With
                  <br /> Big Ideas{" "}
                </h1>
                <p className="mb-0">
                  Talk To Our Experts and Get Your Dream Home Done. If you dream
                  of designing <br />
                  a new home that takes full advantage of the unique geography
                  and <br />
                  views of land that you love
                </p>
              </div>
              <button className="btn btn-hero btn-lg small" role="button">
                Explore Now
              </button>
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="slide-2">
            <div className="overlay" />
          </div>
          <div className="hero">
            <div className="hero d-none d-md-block">
              <div className="text-left">
                <div className="text-left">
                  <h1 className="text-white">
                    We Do Big Things With
                    <br /> Big Ideas{" "}
                  </h1>
                  <p className="mb-0">
                    Talk To Our Experts and Get Your Dream Home Done. If you
                    dream of designing <br />
                    a new home that takes full advantage of the unique geography
                    and <br />
                    views of land that you love
                  </p>
                </div>
                <button className="btn btn-hero btn-lg small" role="button">
                  Explore Now
                </button>
              </div>
            </div>
          </div>
        </Carousel.Item>
        <Carousel.Item>
          <div className="slide-3">
            <div className="overlay" />
          </div>
          <div className="hero d-none d-md-block">
            <div className="text-left">
              <div className="text-left">
                <h1 className="text-white">
                  We Do Big Things With
                  <br /> Big Ideas{" "}
                </h1>
                <p className="mb-0">
                  Talk To Our Experts and Get Your Dream Home Done. If you dream
                  of designing <br />
                  a new home that takes full advantage of the unique geography
                  and <br />
                  views of land that you love
                </p>
              </div>
              <button className="btn btn-hero btn-lg small" role="button">
                Explore Now
              </button>
            </div>
          </div>
        </Carousel.Item>
      </Carousel>
  </div>
  {/* SLIDER END */}
    </>
  )
}

export default Slider