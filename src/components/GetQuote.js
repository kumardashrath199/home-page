import React from 'react'
import Link from 'next/link';
const GetQuote = () => {
  return (
   <>
     {/* OUR VALUE SECTION START */}
  <div className="section-full site-bg-primary py-3">
    <div className="container">
      <div className="row align-items-md-center justify-content-md-between">
        <div className="col-lg-8 col-md-8 our-value-left d-flex align-items-center pb-3 pb-md-0">
          <h2 className="text-white font-weight-400 m-auto">
            Consulting And Estimate For Your Project,{" "}
            <span>
              <Link href="jghjh" className="text-black text-decoration-underline">
                {" "}
                Contact Us Today
              </Link>{" "}
            </span>
          </h2>
        </div>
        <div className="col-lg-3 col-md-4">
          <div className="">
            <Link
              href="gfgg"
              className="bg-white btn font-weight-500"
              style={{ color: "#F9B701" }}
            >
              Get a Quote
            </Link>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/* OUR VALUE SECTION  END */}
   </>
  )
}

export default GetQuote