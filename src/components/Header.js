"use client"
import React from 'react'
import Image from 'next/image'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Link from 'next/link';

const Header = () => {
  return (
    <>
    <header className="site-header header-style-1 mobile-sider-drawer-menu">
    <div className="top-bar">
    <div className="container">
        <div className="d-block text-center  d-md-flex justify-content-between align-items-center">
          <div className="wt-topbar-left">Have any question? </div>
          <div className="wt-topbar-right">
            <ul className="list-unstyled e-p-bx">
              <li>
                <Image src="/mail.svg" width={15} height={15} alt="" />{" "}
                <Link href="mailto:mail@digiwhiz.com"> mail@digiwhiz.com </Link>
              </li>
              <li>
                <Image src="/call.svg" width={15} height={15} alt="" />{" "}
                <Link href="tel:+1(654) 321-7654"> (654) 321-7654 </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <Navbar bg="light" expand="lg" className='justi'>
      <Container>
        <Navbar.Brand href="#home" className='logo-header'><Image src="/logo.svg" width={171} height={49} alt="" /></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav header-nav" className='justify-content-end'>
          <Nav className="justify-content-end nav">
            <Nav.Link href="#home" className='fw-bold'>About</Nav.Link>
            <Nav.Link href="#link" className='fw-bold'>Features</Nav.Link>
            <Nav.Link href="#link" className='fw-bold'>Project</Nav.Link>
            <Nav.Link href="#link" className='fw-bold'>Pages</Nav.Link>
            <Nav.Link href="#link" className='fw-bold'>Contact</Nav.Link>
            <Nav.Link href="#link">
                <Image src="/search.svg" width={14} height={14} alt="" />
            </Nav.Link>
           
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    </header>
    </>
  )
}

export default Header