"use client"
import React from 'react'
import Isotope from 'isotope-layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faLink } from '@fortawesome/free-solid-svg-icons'
import Image from 'next/image'
import Link from 'next/link';
const isBrowser = typeof window !== "undefined";

const Feature = () => {
      // init one ref to store the future isotope object
  const isotope = React.useRef()
  // store the filter keyword in a state
  const [filterKey, setFilterKey] = React.useState("*")

  // initialize an Isotope object with configs
  React.useEffect(() => {
    setTimeout(() => {
    isotope.current = new Isotope('.filter-container', {
      itemSelector: '.masonry-item',
      layoutMode: 'fitRows',
    })
    // cleanup
    return () => isotope.current.destroy()
}, 100);
  }, [])

  // handling filter key change
  React.useEffect(() => {
    setTimeout(() => {
    filterKey === '*'
      ? isotope.current.arrange({filter: `*`})
      : isotope.current.arrange({filter: `.${filterKey}`})
}, 100);  
  }, [filterKey])

  const handleFilterKeyChange = key => () => setFilterKey(key)
  return (
    <>
      {/* LATEST PROJECT SECTION START */}
  <div className="section-full bg-white h-auto p-t80">
    <div className="container">
      <div className="section-head text-center">
        <h1 className="text-uppercase ">Featured Works</h1>
        <p>
          Lorem ipsum dolor sit amet, sed dicunt oporteat cu, laboramus
          definiebas cum et. Duo id omnis persequeris neglegentur, facete
          commodo ea usu, possit lucilius sed ei. Esse efficiendi scripserit eos
          ex. Sea utamur iisque salutatus id.Mel autem animal.
        </p>
        <div className="filter-wrap center">
          <ul className="masonry-filter">
            <li className="active">
              <a type='Button' onClick={handleFilterKeyChange('*')}>
                <span> All</span>
              </a>
            </li>
            <li>
              <a type='Button' onClick={handleFilterKeyChange('Building')}>
                Green Building
              </a>
            </li>
            <li>
              <a type='Button' onClick={handleFilterKeyChange('Healthcare')}>
                Healthcare
              </a>
            </li>
            <li>
              <a type='Button' onClick={handleFilterKeyChange('Interior')}>
                Interior Design
              </a>
            </li>
            <li>
              <a type='Button' onClick={handleFilterKeyChange('Office')}>
                Office
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div className="container-fluid">
      <div className="portfolio-wrap mfp-gallery no-col-gap row filter-container">
        {/* COLUMNS 1 */}
        <div className="masonry-item Building col-xl-4 col-lg-4 col-md-6">
          <div className="wt-gallery-bx">
            <div className="wt-thum-bx wt-Image-overlay1 wt-Image-effect zoom-slow">
              <div>
                <Image src="/upperview.svg" alt="" width={500} height={300} />
              </div>
              <div className="overlay-bx d-flex justify-content-between align-items-center px-5">
                <div className="">
                  <h2 className="h5 text-black mb-0">
                    ARCHITECTURE AND DESIGN
                  </h2>
                  <div className="wt-separator-outer px-0 text-left">
                    <div className="wt-separator">
                      <span className="separator-left bg-dark" />
                    </div>
                  </div>
                </div>
                <div className="overlay-icon">
                  <p className='p-2'>
                  <FontAwesomeIcon icon={faPlus} size='xl' style={{color: "#ffffff",}} />
                  </p>
                  <p className="p-2">
                  <FontAwesomeIcon icon={faLink} size='xl' style={{color: "#ffffff",}} />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* COLUMNS 2 */}
        <div className="masonry-item Building col-xl-4 col-lg-4 col-md-6">
          <div className="wt-gallery-bx">
            <div className="wt-thum-bx wt-Image-overlay1 wt-Image-effect zoom-slow wt-Image-effect zoom">
              <div>
                <Image src="/beach.svg" alt="" width={500} height={300} />
              </div>
              <div className="overlay-bx d-flex justify-content-between align-items-center px-5">
                <div className="">
                  <h2 className="h5 text-black mb-0">
                    ARCHITECTURE AND DESIGN
                  </h2>
                  <div className="wt-separator-outer px-0 text-left">
                    <div className="wt-separator">
                      <span className="separator-left bg-dark" />
                    </div>
                  </div>
                </div>
                <div className="overlay-icon">
                  <p className='p-2'>
                  <FontAwesomeIcon icon={faPlus} size='xl' style={{color: "#ffffff",}} />
                  </p>
                  <p className="p-2">
                  <FontAwesomeIcon icon={faLink} size='xl' style={{color: "#ffffff",}} />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* COLUMNS 3 */}
        <div className="masonry-item Healthcare col-xl-4 col-lg-4 col-md-6">
          <div className="wt-gallery-bx">
            <div className="wt-thum-bx wt-Image-overlay1 wt-Image-effect zoom-slow">
            <div>
                <Image src="/house.svg" alt="" width={500} height={300} />
              </div>
              <div className="overlay-bx d-flex justify-content-between align-items-center px-5">
                <div className="">
                  <h2 className="h5 text-black mb-0">
                    ARCHITECTURE AND DESIGN
                  </h2>
                  <div className="wt-separator-outer px-0 text-left">
                    <div className="wt-separator">
                      <span className="separator-left bg-dark" />
                    </div>
                  </div>
                </div>
                <div className="overlay-icon">
                  <p className='p-2'>
                  <FontAwesomeIcon icon={faPlus} size='xl' style={{color: "#ffffff",}} />
                  </p>
                  <p className="p-2">
                  <FontAwesomeIcon icon={faLink} size='xl' style={{color: "#ffffff",}} />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* COLUMNS 4 */}
        <div className="masonry-item Healthcare col-xl-4 col-lg-4 col-md-6">
          <div className="wt-gallery-bx">
            <div className="wt-thum-bx wt-Image-overlay1 wt-Image-effect zoom-slow">
              <div>
                <Image src="/beach.svg" alt="" width={500} height={300} />
              </div>
              <div className="overlay-bx d-flex justify-content-between align-items-center px-5">
                <div className="">
                  <h2 className="h5 text-black mb-0">
                    ARCHITECTURE AND DESIGN
                  </h2>
                  <div className="wt-separator-outer px-0 text-left">
                    <div className="wt-separator">
                      <span className="separator-left bg-dark" />
                    </div>
                  </div>
                </div>
                <div className="overlay-icon">
                  <p className='p-2'>
                  <FontAwesomeIcon icon={faPlus} size='xl' style={{color: "#ffffff",}} />
                  </p>
                  <p className="p-2">
                  <FontAwesomeIcon icon={faLink} size='xl' style={{color: "#ffffff",}} />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* COLUMNS 5 */}
        <div className="masonry-item Interior col-xl-4 col-lg-4 col-md-6">
          <div className="wt-gallery-bx">
            <div className="wt-thum-bx wt-Image-overlay1 wt-Image-effect zoom-slow">
              <div>
                <Image src="/upperview.svg" alt="" width={500} height={300} />
              </div>
              <div className="overlay-bx d-flex justify-content-between align-items-center px-5">
                <div className="">
                  <h2 className="h5 text-black mb-0">
                    ARCHITECTURE AND DESIGN
                  </h2>
                  <div className="wt-separator-outer px-0 text-left">
                    <div className="wt-separator">
                      <span className="separator-left bg-dark" />
                    </div>
                  </div>
                </div>
                <div className="overlay-icon">
                  <p className='p-2'>
                  <FontAwesomeIcon icon={faPlus} size='xl' style={{color: "#ffffff",}} />
                  </p>
                  <p className="p-2">
                  <FontAwesomeIcon icon={faLink} size='xl' style={{color: "#ffffff",}} />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* COLUMNS 6 */}
        <div className="masonry-item Office col-xl-4 col-lg-4 col-md-6">
          <div className="wt-gallery-bx">
            <div className="wt-thum-bx wt-Image-overlay1 wt-Image-effect zoom-slow">
              <div>
                <Image src="/park.svg" alt="" width={500} height={300} />
              </div>
              <div className="overlay-bx d-flex justify-content-between align-items-center px-5">
                <div className="">
                  <h2 className="h5 text-black mb-0">
                    ARCHITECTURE AND DESIGN
                  </h2>
                  <div className="wt-separator-outer px-0 text-left">
                    <div className="wt-separator">
                      <span className="separator-left bg-dark" />
                    </div>
                  </div>
                </div>
                <div className="overlay-icon">
                  <p className='p-2'>
                  <FontAwesomeIcon icon={faPlus} size='xl' style={{color: "#ffffff",}} />
                  </p>
                  <p className="p-2">
                  <FontAwesomeIcon icon={faLink} size='xl' style={{color: "#ffffff",}} />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/* LATEST PROJECT SECTION END */}
    </>
  )
}

export default Feature