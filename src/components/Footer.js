import React from 'react'
import Link from 'next/link';
export const Footer = () => {
  return (
    <>
    <footer className="site-footer footer-light">
  <div className="footer-bottom overlay-wraper">
    <div className="overlay-main" />
    <div className="container p-t30">
      <div className="row justify-content-between">
        <div className="wt-footer w-auto">
          <span className="copyrights-text">© DIGIWHIZ 2019</span>
        </div>
        <div className="wt-footer w-auto">
          <ul className="copyrights-nav pull-right">
            <li>
              <Link href="hg">Terms &amp; Condition</Link>
            </li>
            <li>
              <Link href="vc">Privacy Policy</Link>
            </li>
            <li>
              <Link href="vcb">Contact Us</Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>
    </>
  )
}
