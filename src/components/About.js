import React from 'react'
import Image from 'next/image'
import Link from 'next/link';

const About = () => {
  return (
   <>
  <div className="section-full p-t80 p-b50 bg-white">
    <div className="container-fluid px-0">
      <div className="row align-items-center bg-gray  ">
        <div className="col-lg-6 col-md-6 px-md-0">
          <div className="about-pic">
            <Image src="/about.svg" alt="" className="img-responsive" width={700}  height={700}/>
          </div>
        </div>
        <div className="col-lg-6 col-md-6 m-b30 bg-gray">
          <div className="about-types row px-md-0">
            <div className="col-md-9 col-sm-12 p-tb20">
              <div className="wt-icon-box-wraper left p-l20 d-flex align-items-center">
                <div className="icon site-text-primary pe-3">
                  <Link href="gfhg" className="icon-cell p-t5 center-block">
                    <Image
                      src="/building.svg"
                      alt=""
                      className="img-responsive"
                      width={69} height={70}
                    />
                  </Link>
                </div>
                <div className="icon-content ">
                  <h5 className="wt-tilte text-uppercase m-b0">
                    GENERAL BUILDER
                  </h5>
                  <p className="small">
                    Lorem ipsum dolor sit amet, sed dicunt oporteat cuum Tuo
                    iomnis persequeris neglegentur, facete commodo ea use possit
                    lucilius.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-9 col-sm-12 p-tb20 ">
              <div className="wt-icon-box-wraper left  p-l20 d-flex align-items-center">
                <div className="icon site-text-primary pe-3">
                  <Link href="hgfhg" className="icon-cell p-t5 center-block">
                    <Image
                      src="/home.svg"
                      alt=""
                      className="img-responsive"
                      width={65} height={70}
                    />
                  </Link>
                </div>
                <div className="icon-content">
                  <h5 className="wt-tilte text-uppercase m-b0">
                    HOUSE EXTENSIONS
                  </h5>
                  <p className="small">
                    Lorem ipsum dolor sit amet, sed dicunt oporteat cuum Tuo
                    iomnis persequeris neglegentur, facete commodo ea use possit
                    lucilius.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-9 col-sm-12 p-tb20 ">
              <div className="wt-icon-box-wraper left  p-l20 d-flex align-items-center">
                <div className="icon site-text-primary pe-3">
                  <Link href="gfhg" className="icon-cell p-t5 center-block">
                    <Image
                      src="/paper.svg"
                      alt=""
                      className="img-responsive"
                      width={67} height={70}
                    />
                  </Link>
                </div>
                <div className="icon-content">
                  <h5 className="wt-tilte text-uppercase m-b0 ">
                    REFURBISHMENT
                  </h5>
                  <p className="small">
                    Lorem ipsum dolor sit amet, sed dicunt oporteat cuum Tuo
                    iomnis persequeris neglegentur, facete commodo ea use possit
                    lucilius.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
   </>
  )
}

export default About