import Image from 'next/image'
import styles from './page.module.css'
import 'bootstrap/dist/css/bootstrap.css'
import './globals.css'
import Header from '../components/Header'
import Slider from '../components/Slider'
import GetQuote from '../components/GetQuote'
import About from '../components/About'
import Feature from '../components/Feature'
import Employee from '../components/Employee'
import { Footer } from '../components/Footer'
// import $ from "jquery"

export default function Home() {
  return (
    <>
    <div className="page-wraper">
        <Header/>
        <Slider/>
        <GetQuote/>
        <About/>
        <Feature/>
        <Employee/>
    </div>
    <Footer/>
  </>
  )
}
