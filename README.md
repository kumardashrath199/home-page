This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

Prerequisites
You will need Node.js version 8.0 or greater installed on your system.

Setup
Get the code by either cloning this repository using git

git clone https://gitlab.com/kumardashrath199/home-page.git
... or downloading source code code as a zip archive.

Once downloaded, open the terminal in the project directory, and install dependencies with:

npm install

Then start the app with:

npm run dev
The app should now be up and running at http://localhost:3000 🚀


